import React, {Component} from 'react';
import Button from "./components/Button/Button.jsx";
import Modal from "./components/Modal/Modal.jsx"
import CenteringWrapper from "./components/CenteringWrapper/CenteringWrapper";

class App extends Component {
    constructor(props) {
        super(props);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }
    state = {
        modalShown: null,
    }

    openModal(modalName) {
        this.setState({modalShown: modalName});
    }
    closeModal() {
        this.setState({modalShown: null});
    }

    render() {
        const s = this.state;
        return (
            <>
                <CenteringWrapper>
                    <Button
                        onClick={() => this.openModal('modalSuccess')}
                        text={'Show Good Modal'}>
                    </Button>
                    <Button
                        onClick={() => this.openModal('modalAlert')}
                        text={'Show Bad Modal'}>
                    </Button>
                </CenteringWrapper>


                <Modal
                    show={s.modalShown === 'modalAlert'}
                    modalType={s.modalShown}
                    header={'React is the worst framework ever'}
                    text={"You just can't disagree with it"}
                    closeButton={false}
                    closerFn={this.closeModal}
                    actions={<Button onClick={this.closeModal} bgColor={'indianred'} text={'I agree'}/>}
                />
                <Modal
                    show={s.modalShown === 'modalSuccess'}
                    modalType={s.modalShown}
                    header={'React is the best framework ever'}
                    text={"It is just the truth"}
                    closeButton={true}
                    closerFn={this.closeModal}
                    actions={<Button onClick={this.closeModal} bgColor={'green'} text={'Indeed'}/>}
                />
            </>

        );
    }
}

export default App;