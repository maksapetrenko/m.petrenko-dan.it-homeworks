import React from 'react';
import Button from "../Button/Button";
import CenteringWrapper from "../CenteringWrapper/CenteringWrapper";
import OutsideTrigger from "./OutsideTrigger/OutsideTrigger";
import styled from "styled-components";

const modalTypeStyles = {
    modalSuccess: `
        color: #0f5132;
        background-color: #d1e7dd;
    `,
    modalAlert: `
        color: #842029;
        background-color: #f8d7da;
    `,
};

const ModalBody = styled.aside`
  position: relative;
  padding: 1rem 1rem;
  margin-bottom: 1rem;
  border: 1px solid transparent;
  border-radius: .25rem;
  z-index: 1;
  ${props => modalTypeStyles[props.modalType]}
`

const ModalHeader = styled.h3`
  position: relative;
  color: inherit;
  margin-top: 0;
  ${props => props.closeButton && 'padding-right:2.5em'}
`

const Hr = styled.hr`
  margin: 1rem 0;
  color: inherit;
  background-color: currentColor;
  border: 0;
  opacity: .25;
  height: 1px;
`

class Modal extends React.Component {
    render() {
        const {show, header, closeButton, actions, text, closerFn, modalType, bgColor} = this.props;
        const modalCloseButtonStyles = {
            position: 'absolute',
            top: 0,
            right: 0,
            fontWeight: 700,
        };

        if (show) return (
            <CenteringWrapper>
                <ModalBody modalType={modalType}>
                    <ModalHeader closeButton={closeButton}>
                        {header}
                        {closeButton && <Button
                            onClick={closerFn}
                            style={modalCloseButtonStyles}
                            bgColor={bgColor}
                            text={'X'}>
                        </Button>}
                    </ModalHeader>
                    <Hr/>
                    <p>{text}</p>
                    <div>{actions}</div>
                </ModalBody>
                <OutsideTrigger onClick={closerFn}/>
            </CenteringWrapper>
        )
        else return null;
    }
}

export default Modal;