import styled from "styled-components";

const OT = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.3);
  z-index: 0;`

const OutsideTrigger = (props) => <OT onClick={props.onClick}/>

export default OutsideTrigger;