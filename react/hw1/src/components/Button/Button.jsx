import styled from "styled-components";

const Btn = styled.button`
  border: 1px solid gray;
  background-color: ${props => props.bgColor || ''};
  padding: .375rem .75rem;
  font-size: 1rem;
  border-radius: .25rem;
  transition: 0.5s background-color;
  cursor: pointer;
  ${props => props.styles};
  &:hover {
    background-color: ${props => props.bgColor};
  }
`

const Button = (props) => {
    const {text, onClick, style, bgColor} = props;
    return (
        <Btn
            styles={style}
            bgColor={bgColor}
            onClick={onClick}>
            {text}
        </Btn>
    );
}

export default Button;