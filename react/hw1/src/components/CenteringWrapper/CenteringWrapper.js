import React, {Component} from 'react';
import styled from "styled-components";

const CW = styled.div`
  width: 100vw;
  display: flex;
  justify-content: space-around;
  padding: 1em;
  margin: auto;
`

class CenteringWrapper extends Component {
    render() {
        return (
            <CW>
                {this.props.children}
            </CW>
        );
    }
}

export default CenteringWrapper;