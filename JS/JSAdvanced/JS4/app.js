const moviesList = document.getElementsByClassName('episodes-list')[0];


function renderMovieCard({episodeId, name, openingCrawl}) {
    const movLi = document.createElement('li');
    movLi.classList.add(`episodes-list-item`)
    movLi.dataset.episodeId = episodeId;
    movLi.innerHTML =
        `<h3 class="movie-title">Episode ${episodeId}: ${name}</h3>
    <p class="movie-description">${openingCrawl}</p>
        <h4 class="characters-list-title">Characters:</h4>
        <div class="loading-placeholder"></div>
    <ul class="characters-list"></ul>`
    moviesList.append(movLi);
    return movLi.getElementsByClassName('characters-list')[0]
}

function renderMovieChars(list, DOMulLink) {
    const listDF = new DocumentFragment();
    list.forEach(char => {
        const li = document.createElement('li');
        li.innerText = char;
        li.classList.add('character');
        listDF.append(li);
    })
    DOMulLink.closest('.episodes-list-item').getElementsByClassName('loading-placeholder')[0].remove();
    DOMulLink.append(listDF);
}

function GET(url) {
    return fetch(url).then(resp => resp.json())
}

(function displayMoviesGeneralInfo() {
    GET('https://ajax.test-danit.com/api/swapi/films')
        .then(movArr => {
            movArr.forEach(movie => {
                const movieCharEmptyUl = renderMovieCard(movie);
                Promise.allSettled(movie.characters.map(url => GET(url)))
                    .then(results => {
                        const charList = [];
                        results.forEach((result) => {
                            result.status === "fulfilled"
                                ? charList.push(result.value.name)
                                : charList.push(`Can't retrieve that char...`);
                        });
                        renderMovieChars(charList, movieCharEmptyUl)
                    });
            })
        })
})()