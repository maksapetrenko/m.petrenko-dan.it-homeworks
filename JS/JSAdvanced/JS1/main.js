class Employee {
    constructor(name,age,salary) {
        [this._name,this._age,this._salary] = [...arguments];
    }
    get name() {
        return this._name;
    }
    set name(str) {
        this._name = str;
    }
    get age() {
        return this._age;
    }
    set age(num) {
        this._age = num;
    }
    get salary() {
        return this._salary;
    }
    set age(num) {
        this._salary = num;
    }
}

const peteBeforeDanIt = new Employee('Peter', 22, 400);
console.log(peteBeforeDanIt);

class Programmer extends Employee {
    constructor(name,age,salary,lang) {
        super(name,age,salary);
        this._lang = [...lang];
    }
    get salary() {
        return this._salary*3;
    }
}

const peteAfterDanIt = new Programmer('Peter', 23, 1200, ['JS','Ruby','PHP']);
const ivanAfterDanIt = new Programmer('Ivan', 33, 1500, ['C++','Python']);
const sergeiTheGreatFrontender = new Programmer('Sergei', 33, 2000, ['JS','HTML','CSS','EnglishB2']);


console.log(
    peteAfterDanIt,
    ivanAfterDanIt,
    sergeiTheGreatFrontender,
    'Sergei gets: '+sergeiTheGreatFrontender.salary
);