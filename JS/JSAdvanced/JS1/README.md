### Как работает прототипное наследование в Javascript?

Прототипное наследование - это механизм, который устанавливает взаимосвязи наследственности между прототипами функций-конструкторов (или классов). Благодаря этому, экземпляр, получаемый как результат вызова некоего конструктора, может обращаться к методам и свойствам, определённых не только для этого конструктора, но и для предыдущих в "генеалогическом древе" этих конструкторов. 

Пример: на [] можно вызвать метод .map, который определён не для экземпляра, а для конструктора, но также можно вызвать метод .keys, который определён в прототипе конструктора Object, доступ к которому вызов [].keys() получит через цепочку прототипов.
