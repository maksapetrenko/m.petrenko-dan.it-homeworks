$(document).ready(() => {
    $('#back-to-top-button').click(() => {
        $('html, body').animate({scrollTop: 0}, "slow");
    });

    $('.nav-menu').click((event) => {
        if (event.target.tagName === 'A') {         //там есть 3 пикселя, когда оно попадает на тело меню, и выбрасывает
            $('html, body').animate({       // ошибку
                scrollTop: $(event.target.getAttribute('href'))
                        .offset().top
                    - $('.nav-menu').height()
                    + 1
            }, 1000);
        }
    });


    for (const elem of $('.hide-section-btn').siblings('.section-heading')) {
        const jElem = $(elem);
        jElem.css('margin-bottom', parseFloat(jElem.css('margin-bottom'))/2);
        jElem.attr('data-hidden','false')
    }
    $('.hide-section-btn').siblings('.section-content-container').attr('data-hidden','false')
    //можно было прописать вручную, но мне лень. +, потренировал jQuery.

    $('.hide-section-btn').click((event) => {
        const btn = event.target;
        const btnTarget = $(btn).siblings('.section-content-container');

        btnTarget.slideToggle(1000);

        setTimeout(()=> {
            if (btnTarget.attr('data-hidden') === 'true') {
                btnTarget.attr('data-hidden', 'false');
                $(btn).text('Hide section');
            }
            else if (btnTarget.attr('data-hidden') === 'false') {
                btnTarget.attr('data-hidden', 'true');
                $(btn).text('Show section');
            }
        },1000)
    });

    $(window).scroll(() => {
        if ($(window).scrollTop() > 800) {
            $('#back-to-top-button').fadeIn('1000');
        } else {
            $('#back-to-top-button').fadeOut('1000');
        }
    });


});

console.log(true === 'true')

