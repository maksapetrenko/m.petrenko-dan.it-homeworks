function calcInput (number1, number2, operation) {
    switch (operation) {
        case '+':
                return (number1 + number2);
        case '-':
            return (number1 - number2);
        case '/':
            return (number1 / number2);
        case '*':
            return (number1 * number2);
        default:
            alert('Invalid operation!');
    }
}
const firstNumber = (+prompt('Enter the first number'));
const secondNumber = (+prompt('Enter the second number'));
const mathOperation = (prompt('Enter the preferred operation: +, -, /, *'));
console.log(calcInput(firstNumber,secondNumber,mathOperation));
