const buttons = document.getElementsByClassName('btn');
document.addEventListener('keypress', (event) => {
    [...buttons].find((btn) => btn.classList.value.includes('highlighted'))
        ?.classList.remove('highlighted');
    [...buttons].find((btn) => btn.innerText.toLowerCase() === event.key.toLowerCase())
        .classList.add('highlighted');

})