let userAge = (+prompt('Enter your age') || '');
let userName = (prompt('Enter your name') || '');

while (!userAge || userAge < 1) {
    userAge = (+prompt('Because the data is missing or invalid, please, enter your age once more', userAge + '') || '');
}
while (!userName.trim() || +userName) {
    userName = (prompt('Because the data is missing or invalid, please, enter your name once more', userName + '') || '');
}

if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (18 <= userAge <= 22) {
    confirm('Are you sure you want to continue?') ? alert(`Welcome, ${userName}`) : alert('You are not allowed' +
        ' to visit this website');
} else if (userAge > 22) {
    alert(`Welcome, ${userName}`);
}