const DOM = {
    items: {
        tabBar: document.getElementsByClassName('tabs')[0],
        tabs: document.getElementsByClassName('tabs')[0].children,
        tabsContent: document.getElementsByClassName('tabs-content')[0].children,
        defaultDescription: document.querySelector('[data-tab-id = defaultChar]'),
    },
    states: {
        activeTab: document.querySelector('.tabs-title.active'),
    },
    actions: {
        init() {
            this.initDatasets();
            this.addSampleDescription();
            this.initListeners();
        },

        initDatasets() {
            [...document.getElementsByClassName('tabs-content')[0].childNodes].forEach((node) => {
                if (node.nodeType === Node.COMMENT_NODE) {
                    const elem = node.nextElementSibling;
                    if (!elem.dataset.tabID) {
                        elem.setAttribute("data-tab-id", `${node.nodeValue.trim()}`);
                    }
                }
            });
            [...DOM.items.tabs].forEach((elem) => {
                const charName = elem.innerText
                elem.setAttribute("data-tab-id", `${charName}`);
            });
        },


        initListeners() {
            DOM.items.tabBar.addEventListener('click', (event) => {
                DOM.actions.activateTab(event.target);
            });
        },


        activateTab(tab) {
            DOM.states.activeTab.classList.remove('active');
            ([...DOM.items.tabsContent]
                    .find((elem) => elem.dataset.tabId === DOM.states.activeTab.dataset.tabId)
                || DOM.items.defaultDescription)
                .classList.remove('active');

            DOM.states.activeTab = tab;

            DOM.states.activeTab.classList.add('active');
            ([...DOM.items.tabsContent]
                    .find((elem) => elem.dataset.tabId === DOM.states.activeTab.dataset.tabId)
                || DOM.items.defaultDescription)
                .classList.add('active');
        },

        addSampleDescription() {
            document.body.querySelector('.tabs-content').insertAdjacentHTML("beforeend", `
            <li data-tab-id="defaultChar">Description will be added soon!</li>`)
            DOM.items.defaultDescription = document.querySelector(
                '[data-tab-id = defaultChar]');
        },
    },
}

DOM.actions.init();