const priceFilter = {
    items: {
        input: document.getElementById('input'),
        warningSign: document.getElementById('warning'),
        warningSignWrapper: document.getElementsByClassName('warning-wrapper')[0],
    },
    actions: {
        initListeners() {
            const It = priceFilter.items;
            It.input.addEventListener('focus', () => {
                It.warningSign.classList.remove('warning-shown');
                It.input.style.color = 'black';
                It.input.style.border = '1px solid green';
            });

            It.input.addEventListener('blur', () => {
                if (!It.input.value) {
                    It.input.style.border = '1px solid grey';
                    return;
                }
                if (this.isPriceCorrect()) {
                    if (!It.priceTag) {
                        It.input.style.border = '1px solid grey';
                        It.input.style.color = 'darkgreen';
                        this.createPriceTag();
                    } else this.redrawExistingPriceTag();
                } else {
                    this.indicateInvalidInput();
                    if (It.priceTag) {
                        It.priceTag.remove();
                        delete It.priceTag;
                    }
                }
            });
        },

        isPriceCorrect() {
            const price = priceFilter.items.input.value.trim();
            return price && parseFloat(price) > 0;
        },

        createPriceTag() {
            const It = priceFilter.items;
            document.body.insertAdjacentHTML('afterbegin',
                `<p id="price-tag">Текущая цена:&nbsp;<span id="price"></span>
                        <i class="far fa-times-circle" 
                           id="cancel-button"></i>
                        </p>\n`)
            It.priceTag = document.getElementById('price-tag');
            It.price = document.getElementById('price');
            It.cancelButton = document.getElementById('cancel-button');

            It.cancelButton.addEventListener('click', () => {
                It.priceTag.remove();
                delete It.priceTag;
            });

            It.price.innerText = `${parseFloat(It.input.value) || -0} грн.`;
        },

        indicateInvalidInput() {
            const It = priceFilter.items;
            It.input.style.border = '1px solid red';
            It.warningSign.innerText = `Please enter correct price`;
            It.warningSign.classList.add('warning-shown');
        },

        redrawExistingPriceTag() {
            priceFilter.items.price.innerText = `${parseFloat(priceFilter.items.input.value)} грн.`;
        }
    },
}

priceFilter.actions.initListeners()