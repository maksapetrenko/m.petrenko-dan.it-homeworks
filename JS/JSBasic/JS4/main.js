"use strict";

function createNewUser(firstName, lastName) {
    let newUser = {
        firstName,
        lastName,
        getLogin() {
            return this.firstName.toLowerCase().slice(0, 1) + this.lastName.toLowerCase();
        },
    }

    Object.defineProperty(newUser, 'firstName', {
        writable: false,
    })
    Object.defineProperty(newUser, 'lastName', {
        writable: false,
    })

    Object.defineProperty(newUser, 'setFirstName', {
        set(value) {
            Object.defineProperty(this, 'firstName', {
                writable: true,
            })
            Object.defineProperty(this, 'firstName', {
                value,
                writable: false,
            })
        }

    });
    Object.defineProperty(newUser, 'setLastName', {
        set(value) {
            Object.defineProperty(this, 'lastName', {
                writable: true,
            })
            Object.defineProperty(this, 'lastName', {
                value,
                writable: false,
            })
        }

    });
    debugger;
    return newUser;
}
let user = createNewUser("Gleb", "Petrenko");
console.log(user.getLogin());

