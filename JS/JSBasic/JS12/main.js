const imageGallery = {
    items: {
        imageWindow: document.querySelector('.images-wrapper'),
        imgList: document.querySelector('.images-wrapper').children,
        playBtn: document.getElementById('play-btn'),
        pauseBtn: document.getElementById('pause-btn'),
        secs: document.getElementById('timer-secs'),
        mili: document.getElementById('timer-miliSecs'),

    },
    timers: {
        standardRotationDuration: 3000,

        remainingOffset: null,
        secsLeft: null,
        miliSecsLeft: null,

        rotationTimer: null,
    },
    actions: {
        changeImage() {
            const activeImage = document.querySelector('.active-image');
            const activeImageIndex =
                [...imageGallery.items.imgList].indexOf(activeImage);

            if (activeImageIndex === imageGallery.items.imgList.length - 1) {
                replaceImage(0);
            } else {
                replaceImage(activeImageIndex + 1);
            }

            function replaceImage(newImageIndex) {
                activeImage
                    .classList.remove('active-image');
                imageGallery.items.imgList[newImageIndex]
                    .classList.add('active-image');
            }
        },

        restartTimer() {
            const Tm = imageGallery.timers;
            const It = imageGallery.items;
            function redrawTimer() {
                if (Tm.remainingOffset <= 10) {
                    imageGallery.actions.changeImage();
                    Tm.remainingOffset = Tm.standardRotationDuration;
                    imageGallery.actions.restartTimer();
                    return;
                }
                Tm.remainingOffset -= 10;
                Tm.secsLeft = Math.trunc(Tm.remainingOffset / 1000);
                Tm.miliSecsLeft = Math.trunc(Tm.remainingOffset / 10) - Tm.secsLeft * 100;

                It.secs.innerText = Tm.secsLeft;
                It.mili.innerText = Tm.miliSecsLeft;
                Tm.rotationTimer = setTimeout(redrawTimer, 10)
            }
            redrawTimer();
        },

        initButtonListener() {
            const It = imageGallery.items;
            It.playBtn.addEventListener('click', () => {
                if (It.imageWindow.dataset['status'] === 'paused') {
                    this.restartTimer();
                    It.imageWindow.dataset['status'] = 'active';
                    It.playBtn.setAttribute('disabled','');
                    It.pauseBtn.removeAttribute('disabled');

                }
            })
            It.pauseBtn.addEventListener('click', () => {
                if (It.imageWindow.dataset['status'] === 'active') {
                    clearTimeout(imageGallery.timers.rotationTimer);
                    It.imageWindow.dataset['status'] = 'paused';
                    It.pauseBtn.setAttribute('disabled','');
                    It.playBtn.removeAttribute('disabled');
                }
            })
        },

        initAll() {
            this.initButtonListener()
            imageGallery.timers.remainingOffset = imageGallery.timers.standardRotationDuration;
            imageGallery.items.secs.innerText = imageGallery.timers.standardRotationDuration/1000;
            imageGallery.items.mili.innerText = (imageGallery.timers.standardRotationDuration.toString().slice(-3,-1));
        }
    },
}

imageGallery.actions.initAll();