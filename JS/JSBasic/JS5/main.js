"use strict";

function createNewUser(firstName, lastName) {
    let newUser = {
        firstName,
        lastName,
        getAge(){
            return (new Date(new Date() - this.birthday).getFullYear()-1970);
        },
        getLogin() {
            return (this.firstName.toLowerCase().slice(0, 1) + this.lastName.toLowerCase());
        },
        getPassword() {
            return (this.firstName.toLowerCase().slice(0, 1) + this.lastName.toLowerCase() + this.birthday.getFullYear());
        },
    }

    let birthdayParams = prompt('Enter your birthday in dd.mm.yyyy format').split('.');
    newUser.birthday = new Date(+birthdayParams[2], +birthdayParams[1]-1, +birthdayParams[0]);
    return newUser;
}
let user = createNewUser("Gleb", "Petrenko");
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());