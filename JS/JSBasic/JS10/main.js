const PassCheck = {
    items: {
        icons: document.getElementsByTagName('i'),
        inputFields: document.getElementsByTagName('input'),
        submitButton: document.getElementsByTagName('button')[0],
        incorrectInputSign: document.getElementsByClassName('incorrect-input')[0],
    },
    content: {
        incorrectInputMessageText: "Нужно ввести одинаковые значения",
    },
    actions: {
        initListeners() {
            this.initIconOnclickListeners();
            this.initSubmitButtonOnclickListener();
            this.initInputFieldsListeners();
        },

        initInputFieldsListeners() {
            [...PassCheck.items.inputFields].forEach(field =>
                field.addEventListener('focus', () => {
                this.hideIncorrectInputWarning();
            }))
        },

        initIconOnclickListeners() {
            [...PassCheck.items.icons].forEach(icon =>
                icon.addEventListener('click', event => {
                    event.preventDefault();
                    icon.previousElementSibling.type === "password"
                        ? icon.previousElementSibling.type = "text"
                        : icon.previousElementSibling.type = "password";
                    PassCheck.actions.changeEyeIconLook(event.target);
                }))
        },

        initSubmitButtonOnclickListener() {
            PassCheck.items.submitButton.addEventListener('click', (event) => {
                event.preventDefault();
                this.submittedDataCheck();
            })
        },

        changeEyeIconLook(icon) {
            icon.classList.toggle('fa-eye');
            icon.classList.toggle('fa-eye-slash');
        },

        submittedDataCheck() {
            if (PassCheck.items.inputFields[0].value &&
                PassCheck.items.inputFields[0].value === PassCheck.items.inputFields[1].value) {
                alert('You\'re welcome!');
            } else {
               this.showIncorrectInputWarning();
            }
        },

        showIncorrectInputWarning() {
            PassCheck.items.incorrectInputSign.innerText =
                PassCheck.content.incorrectInputMessageText;
        },

        hideIncorrectInputWarning() {
            PassCheck.items.incorrectInputSign.innerText = '';
        },
    },
}

PassCheck.actions.initListeners();

