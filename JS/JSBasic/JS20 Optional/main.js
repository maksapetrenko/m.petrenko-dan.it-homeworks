const filterCollection = function (
    arrayToFilter,
    searchQuery,
    strictSearchNeeded,
    ...searchLocations) {
        const searchParams = searchQuery.split(' ');
        const searchResults = new Array(searchParams.length).fill(false);



        if (strictSearchNeeded) return searchResults.every(elem => elem)
        else return searchResults.some(elem => elem)
}


const vehicles = [1,2,2];

filterCollection(
    vehicles,
    'en_US Toyota',
    true,
    'name', 'description', 'contentType.name', 'locales.name', 'locales.description'
)
