
function liInsertMachine(array, elementToModify = document.body) {
    const arrayOfLi = array.map((elem) => `<li>${elem}</li>`);
    elementToModify.insertAdjacentHTML("beforeend", `<ul>${arrayOfLi.join('')}</ul>`);
}

liInsertMachine(
    ["hello", "world", 'Kiev', "Kharkiv", "Odessa", "Lviv"],
    document.getElementById("test-p"));















if (confirm(`Хотите посмотреть дополнительное задание?
(Основное выполнится в любом случае)`)) {
    function liUltimateInsertMachine(array, elementToModify = document.body) {
        const fragmentToInsert = [];

        const listMaker = function (arr) {
            fragmentToInsert.push(`<ul>`);
            arr.forEach(function (elem) {
                if (Array.isArray(elem)) {
                    listMaker(elem)
                } else fragmentToInsert.push(`<li>${elem}</li>`)
            })
            fragmentToInsert.push(`</ul>`);
        }

        listMaker(array);
        elementToModify.insertAdjacentHTML("beforeend", fragmentToInsert.join(''));
    }

    liUltimateInsertMachine(
        ["America", "Ukraine", ['Kiev', ['Left bank', 'Right bank'], "Kharkiv"], "Latvia", "Spain"],
        document.getElementById("ultimate-test-p"));




    document.body.insertAdjacentHTML('beforeend',`<div id="clock">Page will be erased in: <b><span id="timer"></span>s</b></div>`);
    const pageTimer = document.getElementById('timer');

    document.getElementById('clock').style.cssText = `
    background-color: #ffedc2;
    padding: 20px;
    border: 1px solid black;
    width: fit-content;
    position: fixed;
    top: 50px;
    right: 50px;
    `;

    const eraseCountdown = function (secs) {
        if (secs === 0) {
            document.body.innerHTML = '';
            return;
        }
        pageTimer.innerText = secs;
        setTimeout(() => eraseCountdown(--secs), 1000);
    }
    eraseCountdown(5);
}