
const filterBy = (array, dataType) => {
    return array.filter(elem => {
        let typeOfElem = typeof elem;
        if (typeof elem === 'number' && isNaN(elem)) {typeOfElem = 'NaN'}
        if (elem === null) {typeOfElem = 'null'}
        if (Array.isArray(elem)) {typeOfElem = 'array'}
        return typeOfElem === dataType.toLowerCase()
    });
}

console.log(filterBy(
    ['hello',
        undefined,
        'world',
        2389278347932479238472349,
        '23',
        null,
        NaN,
        {a:2, o3:2},
        [1,2],
        Symbol(),
        BigInt(9007199254740992)],    'BigInt'));