const themeCssLink = document.getElementById('theme-file-link')
themeCssLink.href = `themes/${localStorage.theme || themeCssLink.dataset.defaultTheme}.css`;





window.onload = () => {
    const switchBtn = document.getElementById('theme-switch');
    switchBtn.dataset.chosenTheme = localStorage.theme || themeCssLink.dataset.defaultTheme;
    switchBtn.textContent =
        switchBtn.dataset.chosenTheme.charAt(0).toUpperCase()
        + switchBtn.dataset.chosenTheme.slice(1) + ' Theme';
    switchBtn.addEventListener('click', () => {
// themeCssLink.insertAdjacentHTML('afterend', `<link
//           rel="stylesheet"
//           href="themes/dark.css"
//           data-default-theme="dark">`)

        const newTheme = (switchBtn.dataset.chosenTheme) === 'light'
            ? 'dark'
            : 'light'
        localStorage.theme = newTheme;
        themeCssLink.href = `themes/${newTheme}.css`;
        switchBtn.dataset.chosenTheme = newTheme;

        switchBtn.classList.toggle('switch-position-left')
        switchBtn.classList.toggle('switch-position-right')
        switchBtn.textContent = newTheme.charAt(0).toUpperCase() + newTheme.slice(1)+' Theme';
    })
};