const gulp = require('gulp');
const del = require('del');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');
const minifyJS = require('gulp-minify');
const minifyImg = require('gulp-imagemin');
const concat = require('gulp-concat');


gulp.task('del:dist', function() {
    return del(['dist/*']);
});
gulp.task('move:html', () => {
    return gulp
            .src('src/index.html')
            .pipe(gulp.dest('dist/'))
});
gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    browserSync.watch('./dist', browserSync.reload);
});

gulp.task('convert:scss', () => {
    return gulp
        .src('src/styles/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist/styles/'))
});
gulp.task('autoprefix:css', () => {
    return gulp
        .src('dist/styles/*')
        .pipe(autoprefixer())
        .pipe(gulp.dest('dist/styles'))
});
gulp.task('minify:css', () => {
    return gulp
        .src('dist/styles/*')
        .pipe(cleanCss({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/styles'))
});
gulp.task('concat:js', () => {
    return gulp.src('./src/scripts/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./dist/scripts'))
});
gulp.task('minify:js', () => {
    return gulp
        .src('./dist/scripts/main.js')
        .pipe(minifyJS())
        .pipe(gulp.dest('./dist/scripts'))
});
gulp.task('del:excessive-js', function() {
    return del(['dist/scripts/main.js']);
});
gulp.task('minify:img', () => {
    return gulp.src('src/images/**/*')
        .pipe(minifyImg())
        .pipe(gulp.dest('dist/imgs'))
});

gulp.task('process:css', gulp.series([
    'convert:scss',
    'autoprefix:css',
    'minify:css',
]));

gulp.task('process:js', gulp.series([
        'concat:js',
        'minify:js',
        'del:excessive-js'
    ])
    );
gulp.task('build', gulp.series([
        'del:dist',
        'move:html',
        'process:css',
        'process:js',
        'minify:img'
    ]))

gulp.task('watch', function () {
    gulp.watch('src/index.html', gulp.series('move:html'))
    gulp.watch('src/styles/**/*', gulp.series('process:css'))
    gulp.watch('src/scripts/**/*', gulp.series('process:js'))
    gulp.watch('src/images/**/*', gulp.series('minify:img'))
})

gulp.task('update', gulp.parallel('watch','server'))

gulp.task('dev', gulp.series([
        'build',
        'update'
        ]))
